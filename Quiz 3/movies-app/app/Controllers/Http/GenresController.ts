import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'

export default class GenresController {
  public async index({ response }: HttpContextContract) {
    let genres = await Database.from('genres').select('id', 'name')
    response.status(200).json({ message: 'Success', data: genres })
  }

  public async store({ request, response }: HttpContextContract) {
    let newGenresID = await Database.table('genres').returning('id').insert({
      name: request.input('name')
    })
    response.created({ message: 'Success', genres_id: newGenresID })
  }

  public async show({ response, params }: HttpContextContract) {
    let genre = await Database.from('genres').where('id', params.id).select('id', 'name').first()
    let movie = await Database.from('movies')
      .where('genres.id', params.id)
      .join('genres', 'movies.genres_id', '=', 'genres.id')
      .select({ id: 'movies.id', title: 'movies.title', release_date: 'movies.release_date', resume: 'movies.resume' })
    let result = genre
    result['movies'] = movie
    response.status(200).json({ message: 'Success', data: result })
  }

  public async update({ request, response, params }: HttpContextContract) {
    let id = params.id
    await Database.from('genres').where('id', id).update({
      name: request.input('name'),
    })
    let genre = await Database.from('genres').where('id', params.id).select('id', 'name').first()
    return response.ok({ message: 'Success', data: genre })
  }

  public async destroy({ params, response }: HttpContextContract) {
    let id = params.id
    await Database.from('genres').where('id', id).delete()
    return response.ok({ message: `Delete genres_id: ${id} Success` })
  }
}
