import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'

export default class MoviesController {
  public async index({ response }: HttpContextContract) {
    let movies = await Database.from('movies')
      .join('genres', 'movies.genres_id', '=', 'genres.id')
      .select({ id: 'movies.id', title: 'movies.title', resume: 'movies.resume', release_date: 'movies.release_date', genre: 'genres.name' })
    response.status(200).json({ message: 'Success', data: movies })
  }

  public async store({ request, response }: HttpContextContract) {
    let newMoviesID = await Database.table('movies').returning('id').insert({
      title: request.input('title'),
      resume: request.input('resume'),
      release_date: request.input('release_date'),
      genres_id: request.input('genre_id'),
    })
    response.created({ message: 'Success', movies_id: newMoviesID })
  }

  public async show({ response, params }: HttpContextContract) {
    let movies = await Database.from('movies')
      .join('genres', 'movies.genres_id', '=', 'genres.id')
      .where('movies.id', params.id)
      .select({ id: 'movies.id', title: 'movies.title', resume: 'movies.resume', release_date: 'movies.release_date', genre: 'genres.name' })
      .first()
    response.status(200).json({ message: 'Success', data: movies })
  }

  public async update({ request, response, params }: HttpContextContract) {
    let id = params.id
    await Database.from('movies').where('id', id).update({
      title: request.input('title'),
      resume: request.input('resume'),
      release_date: request.input('release_date'),
      genres_id: request.input('genre_id'),
    })
    let movie = await Database.from('movies')
      .where('movies.id', params.id)
      .join('genres', 'movies.genres_id', '=', 'genres.id')
      .select({ id: 'movies.id', title: 'movies.title', resume: 'movies.resume', release_date: 'movies.release_date', genre: 'genres.name' })
      .first()
    return response.ok({ message: 'Success', data: movie })
  }

  public async destroy({ params, response }: HttpContextContract) {
    let id = params.id
    await Database.from('movies').where('id', id).delete()
    return response.ok({ message: `Delete movies_id: ${id} Success` })
  }
}
