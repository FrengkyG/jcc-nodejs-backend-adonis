import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Casts extends BaseSchema {
  protected tableName = 'casts'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.integer('movies_id').unsigned()
      table.foreign('movies_id').references('movies.id')
      table.integer('actors_id').unsigned()
      table.foreign('actors_id').references('actors.id')
      table.timestamps(true, true)
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
