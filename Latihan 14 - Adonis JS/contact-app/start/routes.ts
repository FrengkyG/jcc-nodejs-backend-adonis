/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
}).as('home')

// Route.get('/testing/:b', async ({ params, request }: HttpContextContract) => {
//   let a: number = 1
//   console.log(request.param('b'))
//   console.log(request.url())
//   console.log(request.completeUrl())
//   console.log(request.qs())
//   return { test: "testing contact app", total: a + parseInt(params.b) }
// }).as('testing')

// Route.get('/contacts', 'ContactsController.index').as('contacts.index')
// Route.post('/contacts', 'ContactsController.store').as('contacts.store')
// Route.get('/contacts/:id', 'ContactsController.show').as('contacts.show')
// Route.put('/contacts/:id', 'ContactsController.update').as('contacts.update')
// Route.delete('/contacts/:id', 'ContactsController.destroy').as('contacts.delete')

Route.resource('contacts', 'ContactsController').apiOnly()