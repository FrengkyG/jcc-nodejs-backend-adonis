import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
// import CreateContactValidator from 'App/Validators/CreateContactValidator'
import Contact from 'App/Models/Contact'

export default class ContactsController {
   public async index({ request, response }: HttpContextContract) {
      if (request.qs().name) {
         let name = request.qs().name
         // Cara Query Builder
         // let contactsFiltered = await Database.from('contacts').select('id', 'phone', 'email', 'name').where('name', name)\
         // cara ORM
         let contactsFiltered = await Contact.findBy('name', name)
         response.status(200).json({ message: 'Success', data: contactsFiltered })
      } else {
         // Cara Query Builder
         // let contacts = await Database.from('contacts').select('id', 'phone', 'email', 'name')
         // Cara model ORM
         let contacts = await Contact.all()
         response.status(200).json({ message: 'Success', data: contacts })
      }
   }

   public async store({ request, response }: HttpContextContract) {
      try {
         // const payload = await request.validate(CreateContactValidator)\
         // Menggunakan Query Builder
         // let newUserID = await Database.table('contacts').returning('id').insert({
         //    name: request.input('name'),
         //    email: request.input('email'),
         //    phone: request.input('phone')
         // })

         // Menggunakan Model
         let newContact = new Contact()
         newContact.name = request.input('name')
         newContact.email = request.input('email')
         newContact.phone = request.input('phone')

         await newContact.save()
         response.created({ message: 'created!', newId: newContact.id })
      } catch (error) {
         response.unprocessableEntity({ errors: error.messages })
      }
   }

   public async show({ response, params }: HttpContextContract) {
      // Cara Query Builder
      // let contact = await Database.from('contacts').where('id', params.id).select('id', 'phone', 'email', 'name').first()

      // Cara Model ORM
      let contact = await Contact.find(params.id)
      return response.ok({ message: 'Success get contact with id', data: contact })
   }

   public async update({ request, response, params }: HttpContextContract) {
      let id = params.id

      // Cara Query Builder 
      // await Database.from('contacts').where('id', id).update({
      //    email: request.input('email'),
      //    name: request.input('name'),
      //    phone: request.input('phone')
      // })

      // Cara Model ORM
      let contact = await Contact.findOrFail(id)
      contact.email = request.input('email')
      contact.name = request.input('name')
      contact.phone = request.input('phone')

      contact.save()

      return response.ok({ message: 'Success' })
   }

   public async destroy({ params, response }: HttpContextContract) {
      let id = params.id
      // Cara Query buiilder
      // await Database.from('Contacts').where('id', id).delete()

      // Cara model ORM
      let contact = await Contact.findOrFail(id)
      await contact.delete()
      return response.ok({ message: 'Success' })
   }
}
