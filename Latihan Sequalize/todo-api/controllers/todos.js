const { Todo } = require('../models')
const todo = require('../models/todo')

class todoController {
   static async findAll(req, res) {
      let todos = await Todo.findAll()
      res.status(200).json({status: 'sucess', data: todos})
   }
   static async store(req,res){
      //dapet data melalui req.body
      let task = req.body.task
      let dueDate = req.body.dueDate

      //simpan data
      //cara 1. Gunakan instance
      // let newTodo = Todo.build({task: task, dueDate: dueDate})
      // await newTodo.save()

      //cara 2. Gunakan metode create
      const todo1 = await Todo.create({task: task, dueDate: dueDate})
      res.status(201).json({status: 'Success', data:'data berhasil di create'})
   }

   static async show(req,res){
      let id = req.params.id
      // let todo = await Todo.findAll({
      //    where: {
      //       id: id
      //    }
      // })
      // cara lain
      let todo = await Todo.findByPk(id)
      res.status(200).json({status: 'success', data: todo})
   }

   static async update(req,res){
      let task = req.body.task
      let dueDate = req.body.dueDate
      await Todo.update({
         task: task,
         dueDate : dueDate
      },{
         where: {
            id: req.params.id
         }
      })

      res.status(200).json({status: 'sucess', data: 'Data berhasil di update'})
   }

   static async destroy(req,res){
      await Todo.destroy({
         where:{
            id: req.params.id
         }
      })

      res.status(200).json({status: 'sucess', data: 'data berhasil di delete'})
   }
}


module.exports = todoController