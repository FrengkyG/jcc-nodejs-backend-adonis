var express = require('express');
var router = express.Router();
const todoController = require('../controllers/todos')

/* GET users listing. */
router.get('/', todoController.findAll);
router.post('/', todoController.store);
router.get('/:id', todoController.show);
router.put('/:id', todoController.update);
router.delete('/:id', todoController.destroy)

module.exports = router;
