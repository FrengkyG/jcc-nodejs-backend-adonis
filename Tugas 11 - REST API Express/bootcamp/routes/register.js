var express = require('express');
var router = express.Router();

var RegisterController = require('../controllers/register')

router.post('/', RegisterController.register)

module.exports = router;
