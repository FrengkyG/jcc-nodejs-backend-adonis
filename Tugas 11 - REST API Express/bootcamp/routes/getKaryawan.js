var express = require('express');
var router = express.Router();

var RegisterController = require('../controllers/register')

router.get('/', RegisterController.getKaryawan)

router.post('/:name/siswa', RegisterController.addSiswa)
module.exports = router;
