const fs = require('fs')

class RegisterController{
   static register(req,res){
      fs.readFile('data.json', (err,data) => {
         if(err){
            res.status(400).json({"errors" : "error membaca data"})
         } else {
            let existingData = JSON.parse(data)
            let { name, password, role} = req.body
            let newUser = { name, password, role}
            if(role == 'admin'){
               let isAdmin = false
               for(let i=0;i<existingData.length;i++){
                  if(existingData[i].role == 'admin'){
                     isAdmin = true
                  }
               }

               if(isAdmin == true){
                  res.status(406).json({message: "Sudah terdapat Admin"})
               } else {
                  existingData.push(newUser)
                  newUser['isLogin'] = false
                  fs.writeFile('data.json', JSON.stringify(existingData), (err)=>{
                     if(err) {
                        res.status(400).json({error: "error menyimpan data"})
                     } else {
                        res.status(201).json({message: "berhasil register"})
                     }
                  })
               }
            } else {
               existingData.push(newUser)
               newUser['isLogin'] = false
               if(role == 'trainer') {
                  newUser['students'] = []
               }
               fs.writeFile('data.json', JSON.stringify(existingData), (err)=>{
                  if(err) {
                     res.status(400).json({error: "error menyimpan data"})
                  } else {
                     res.status(201).json({message: "berhasil register"})
                  }
               })
            }
         }
      })
   }

   static getKaryawan(req,res){
      fs.readFile('data.json', (err,data) => {
         if(err){
            res.status(400).json({"errors" : "error membaca data"})
         } else {
            let realData = JSON.parse(data)
            res.status(200).json({message: "berhasil get karyawan", data: realData })
         }
      })
   }

   static login(req, res){
      fs.readFile('data.json', (err, data) => {
         if(err){
            res.status(400).json({"errors" : "error membaca data"})
         } else {
            let user = JSON.parse(data)
            let { name, password } = req.body
            let alreadyLogin = false
            for(let i=0;i<user.length;i++){
               if(user[i]['isLogin'] == true){
                  alreadyLogin = true
               }
            }

            if(alreadyLogin == true) {
               res.status(406).json({message: "Anda telah Login"})
            } else {
               for(let i=0;i<user.length;i++){
                  if(String(name) == user[i]["name"] && String(password) == user[i]["password"]){
                     user[i]["isLogin"] = true
                     fs.writeFile('data.json', JSON.stringify(user), (err)=>{
                        if(err) {
                           res.status(400).json({error: "error update login"})
                        } else {
                           res.status(201).json({message: "berhasil login", data: user[i]})
                        }
                     })
                  }
               }
            }
         }
      })
   }

   static addSiswa(req,res){
      fs.readFile('data.json', (err,data) => {
         if(err){
            res.status(400).json({"errors" : "error membaca data"})
         } else {
            let existingData = JSON.parse(data)
            let name = req.body['name']
            let kelas = req.body['class']
            let newStudent = { name, kelas }
            let namaTrainer = req.params['name']
            for(let i=0;i<existingData.length;i++){
               if(existingData[i]["isLogin"] == true && existingData[i]["role"] == "admin") {
                  for(let j=0;j<existingData.length;j++) {
                     if(existingData[j]["name"] == String(namaTrainer)){
                        existingData[j]["students"].push(newStudent)
                        fs.writeFile('data.json', JSON.stringify(existingData), (err)=>{
                           if(err) {
                              res.status(400).json({error: "error menyimpan data"})
                           } else {
                              res.status(201).json({message: "berhasil add siswa", data: existingData})
                           }
                        })
                     } else {
      
                     }
                  }
               } else {
                  console.log("Anda tidak terdaftar sebagai admin")
               }
            }

            
         }

      })
   }
}

module.exports = RegisterController