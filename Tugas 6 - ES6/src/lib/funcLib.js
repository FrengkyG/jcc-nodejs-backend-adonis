export const sapa = (nama) => `halo selamat pagi, ${nama}`

export const convert = (params) => {
   let {nama, domisili, umur} = params
   return {
      nama,
      domisili,
      umur,
   }
}

export const checkScore = (str) => {
   let splitedString = str.split(/\W+/)
   let filteredArray = []
   for(let i=0; i<splitedString.length; i++){
      if(i%2==1){
         filteredArray.push(splitedString[i])
      }
   }

   let [name, kelas, score] = filteredArray
   return {
      name,
      kelas,
      score
   }


}



 export const filterData = (namaKelas) => {
   const data = [
      { name: "Ahmad", class: "adonis"},
      { name: "Regi", class: "laravel"},
      { name: "Bondra", class: "adonis"},
      { name: "Iqbal", class: "vuejs" },
      { name: "Putri", class: "Laravel" }
    ]

    let [...rest] = data
    
    return rest.filter(function(dat){
       return dat.class == namaKelas
    })
   

 }