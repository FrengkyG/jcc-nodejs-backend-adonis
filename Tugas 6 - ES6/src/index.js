import { sapa, convert, checkScore,  filterData} from './lib/funcLib'

let args = process.argv

const data = [
   { name: "Ahmad", class: "adonis"},
   { name: "Regi", class: "laravel"},
   { name: "Bondra", class: "adonis"},
   { name: "Iqbal", class: "vuejs" },
   { name: "Putri", class: "Laravel" }
 ]

switch(args[2]){
   case "sapa":
      console.log(sapa(args[3]))
      break;

   case "convert":
      let nama = args[3]
      let domisili = args[4]
      let umur = args[5]
      console.log(convert({ nama, domisili, umur}))
      break;
   
   case "checkScore":
      let str = args[3]
      console.log(checkScore(str))
      break;

   case "filterData":
      console.log(filterData(args[3]))
      break;

}