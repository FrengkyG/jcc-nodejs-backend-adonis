"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.filterData = exports.checkScore = exports.convert = exports.sapa = void 0;

var sapa = function sapa(nama) {
  return "halo selamat pagi, ".concat(nama);
};

exports.sapa = sapa;

var convert = function convert(params) {
  var nama = params.nama,
      domisili = params.domisili,
      umur = params.umur;
  return {
    nama: nama,
    domisili: domisili,
    umur: umur
  };
};

exports.convert = convert;

var checkScore = function checkScore(str) {
  var splitedString = str.split(/\W+/);
  var filteredArray = [];

  for (var i = 0; i < splitedString.length; i++) {
    if (i % 2 == 1) {
      filteredArray.push(splitedString[i]);
    }
  }

  var name = filteredArray[0],
      kelas = filteredArray[1],
      score = filteredArray[2];
  return {
    name: name,
    kelas: kelas,
    score: score
  };
};

exports.checkScore = checkScore;

var filterData = function filterData(namaKelas) {
  var data = [{
    name: "Ahmad",
    "class": "adonis"
  }, {
    name: "Regi",
    "class": "laravel"
  }, {
    name: "Bondra",
    "class": "adonis"
  }, {
    name: "Iqbal",
    "class": "vuejs"
  }, {
    name: "Putri",
    "class": "Laravel"
  }];
  var rest = data.slice(0);
  return rest.filter(function (dat) {
    return dat["class"] == namaKelas;
  });
};

exports.filterData = filterData;