"use strict";

var _funcLib = require("./lib/funcLib");

var args = process.argv;
var data = [{
  name: "Ahmad",
  "class": "adonis"
}, {
  name: "Regi",
  "class": "laravel"
}, {
  name: "Bondra",
  "class": "adonis"
}, {
  name: "Iqbal",
  "class": "vuejs"
}, {
  name: "Putri",
  "class": "Laravel"
}];

switch (args[2]) {
  case "sapa":
    console.log((0, _funcLib.sapa)(args[3]));
    break;

  case "convert":
    var nama = args[3];
    var domisili = args[4];
    var umur = args[5];
    console.log((0, _funcLib.convert)({
      nama: nama,
      domisili: domisili,
      umur: umur
    }));
    break;

  case "checkScore":
    var str = args[3];
    console.log((0, _funcLib.checkScore)(str));
    break;

  case "filterData":
    console.log((0, _funcLib.filterData)(args[3]));
    break;
}