"use strict";

var _bootcamp = require("./lib/bootcamp");

var _student = require("./lib/student");

var _kelas = require("./lib/kelas");

var sanber = new _bootcamp.Bootcamp("sanbercode");
sanber.createClass("Laravel", "beginner", "abduh");
sanber.createClass("React", "beginner", "abdul");
console.log(sanber.classes);
var names = ["regi", "ahmad", "bondra", "iqbal", "putri", "rezky"];
names.map(function (nama, index) {
  var newStud = new _student.Student(nama);
  var kelas = sanber.classes[index % 2].name;
  sanber.register(kelas, newStud);
  sanber.kelas.graduate(newStud);
}); // menampilkan data kelas dan student nya

sanber.classes.forEach(function (kelas) {
  console.log(kelas);
});
console.log(sanber.classes);
names.map(function (nama, index) {
  var newStud = new _student.Student(nama);
  sanber.newStud.addScore(newStud);
});