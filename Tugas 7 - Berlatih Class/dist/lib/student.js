"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Student = void 0;

var _kelas = require("./kelas");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Student = /*#__PURE__*/function () {
  function Student(name) {
    _classCallCheck(this, Student);

    this._name = name;
    this._scores = [];
    this._finalScores;
  }

  _createClass(Student, [{
    key: "runBatch",
    value: function runBatch(stud) {
      for (var i = 0; i < 4; i++) {
        var scores = Math.floor(Math.random() * 51) + 50;

        stud._scores.push(scores);
      }

      stud._finalScores = (this._scores[0] + this._scores[1] + this._scores[2] + this._scores[3]) / 4;
    }
  }]);

  return Student;
}();

exports.Student = Student;