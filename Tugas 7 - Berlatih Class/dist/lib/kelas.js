"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Kelas = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Kelas = /*#__PURE__*/function () {
  function Kelas(name, level, instructur) {
    _classCallCheck(this, Kelas);

    this._name = name;
    this._student = [];
    this._level = level;
    this._instructor = instructur;
    this._participant = [];
    this._completed = [];
    this._mastered = [];
  }

  _createClass(Kelas, [{
    key: "name",
    get: function get() {
      return this._name;
    }
  }, {
    key: "graduate",
    value: function graduate(stud) {
      if (stud._finalScore < 60) {
        this._participant = this._student;
      } else if (stud._finalScore >= 60 || stud._finalScore < 85) {
        this._completed = this._student;
      } else {
        this._mastered = this._student;
      }
    }
  }]);

  return Kelas;
}();

exports.Kelas = Kelas;