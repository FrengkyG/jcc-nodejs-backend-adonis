"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Bootcamp = void 0;

var _kelas = require("./kelas");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Bootcamp = /*#__PURE__*/function () {
  function Bootcamp(name) {
    _classCallCheck(this, Bootcamp);

    this._name = name;
    this._classes = [];
    var kelas;
  }

  _createClass(Bootcamp, [{
    key: "name",
    get: function get() {
      return this._name;
    }
  }, {
    key: "classes",
    get: function get() {
      return this._classes;
    }
  }, {
    key: "createClass",
    value: function createClass(name, level, instructur) {
      this.kelas = new _kelas.Kelas(name, level, instructur);

      this._classes.push(this.kelas);
    }
  }, {
    key: "register",
    value: function register(kelas, stud) {
      var i = this.kelas._name.indexOf(kelas) + 1;

      this._classes[i]._student.push(stud);
    }
  }]);

  return Bootcamp;
}();

exports.Bootcamp = Bootcamp;