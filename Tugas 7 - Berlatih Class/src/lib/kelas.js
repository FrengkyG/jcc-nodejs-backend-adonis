export class Kelas {
   constructor(name, level, instructur){
      this._name = name
      this._student = []
      this._level = level
      this._instructor = instructur
      this._participant = []
      this._completed = []
      this._mastered = []
   }

   get name(){
      return this._name
   }

   graduate(stud){
      if(stud._finalScore<60){
         this._participant = this._student
      } else if (stud._finalScore>=60 || stud._finalScore<85) {
         this._completed = this._student
      } else {
         this._mastered = this._student
      }
   }

}