import { Kelas } from './kelas'

export class Bootcamp {
   constructor(name){
      this._name = name
      this._classes = []
      var kelas
   }

   get name(){
      return this._name
   }

   get classes(){
      return this._classes
   }

   createClass(name, level, instructur){
      this.kelas = new Kelas(name, level, instructur)
      this._classes.push(this.kelas)
   }

   register(kelas, stud){
      let i = (this.kelas._name.indexOf(kelas))+1     
      this._classes[i]._student.push(stud)

   }
}