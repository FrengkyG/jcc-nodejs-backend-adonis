var express = require('express');
var router = express.Router();

var UserController = require('../controllers/users')
/* GET users listing. */
router.get('/', UserController.findAll)

router.get('/:id', (req, res) => {
  console.log("parameters : ", req.params)
  res.send(`Halo ${req.params.id} USer`)
})

router.post('/register', UserController.register)
module.exports = router;
