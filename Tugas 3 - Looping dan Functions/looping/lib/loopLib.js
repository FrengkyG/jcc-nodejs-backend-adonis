function loopWhile(){
   var hasil = ""
   var i = 2
   hasil += "LOOPING PERTAMA\n"
   while(i<=20){
      hasil += [i] + " - I love coding\n"
      i+=2
   }

   hasil += "LOOPING KEDUA\n";
   i=20
   while(i>0){
      hasil += [i] + " - I will become a mobile developer\n"
      i-=2
   }
   return hasil

}

function loopFor(){
   var jawaban2 = ""
   for (var i=1;i<=20;i++){
      if(i%3==0 && i%2==1){
            var text = " - I Love Coding\n"
      } else if (i%2==0){
            var text = " - Berkualitas\n"
      } else if (i%2==1) {
            var text = " - Santai\n"
      }
      jawaban2 += [i] + text
   }
   return jawaban2
}

function persegiPanjang(panjang, lebar){
   var jawaban3 = "" 
   for(var i=0;i<lebar;i++){
      for(var j=0;j<panjang;j++){
         jawaban3 += "#"
      }
      jawaban3 += "\n"
   }
   return jawaban3
}

function tangga(sisi){
   var jawaban4 = ""
   for(var i=0;i<sisi;i++){
      for(var j=0;j<=i;j++){
         jawaban4 += "#"
      }
      jawaban4 += "\n"
   }
   return jawaban4
}

function catur(sisi){
   var jawaban5 = ""
   for(var i=0;i<sisi;i++){
      for(var j=1;j<=sisi;j++){
         if(i%2==0 && j%2==1){
            jawaban5 += " "
         } else if(i%2==0 && j%2==0){
            jawaban5 += "#"
         } else if(i%2==1 && j%2==1){
            jawaban5 += "#"
         } else if(i%2==1 && j%2==0){
            jawaban5 += " "
         }
      }
      jawaban5 += "\n"
   }
   return jawaban5

}

module.exports = {
   loopWhile: loopWhile,
   loopFor: loopFor,
   persegiPanjang: persegiPanjang,
   tangga: tangga,
   catur: catur
}