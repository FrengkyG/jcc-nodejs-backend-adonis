var loopLib = require('./lib/loopLib.js')

var args = process.argv
var loopFor = loopLib.loopFor
var loopWhile = loopLib.loopWhile
var persegiPanjang = loopLib.persegiPanjang
var tangga = loopLib.tangga
var catur = loopLib.catur

switch(args[2]){
   case 'while':
      console.log(loopWhile())
      break;

   case 'for':
      console.log(loopFor())
      break;
   
   case 'persegiPanjang':
      console.log(persegiPanjang(args[3], args[4]))
      break;
   
   case 'tangga':
      console.log(tangga(args[3]))
      break;

   case 'catur':
      console.log(catur(args[3]))
      break;

   default:
      console.log('Perintah yang Anda berikan salah')
      break;
}