var funcLib = require('./lib/funcLib.js')

var args = process.argv
var teriak = funcLib.teriak
var kalikan = funcLib.kalikan
var introduce = funcLib.introduce

switch(args[2]){
   case "teriak":
      console.log(teriak())
      break;

   case "kalikan":
      var hasilKali = kalikan(args[3], args[4])
      console.log(hasilKali)
      break;

   case "kenalan":
      var perkenalan = introduce(args[3], args[4], args[5], args[6])
      console.log(perkenalan)
      break;

   default:
      console.log('Perintah yang Anda berikan salah')
      break;

}