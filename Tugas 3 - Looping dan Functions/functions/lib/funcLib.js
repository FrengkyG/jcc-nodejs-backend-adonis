function teriak(){
   return "Halo Sanbers!"
}

function kalikan(angka1, angka2) {
   return angka1 * angka2
}

function introduce(name, age, address, hobby){
   return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!"
}

module.exports = {
   teriak: teriak,
   kalikan: kalikan,
   introduce: introduce
}