const { Venue } = require('../models')

class VenueController {
   static async store(req,res){
      let { name, address, phone} = req.body
      const newVenue = await Venue.create({name: name, address: address, phone: phone})
      res.status(201).json({status: 'Success', data: newVenue})
   }

   static async findAll(req, res) {
      let venues = await Venue.findAll()
      res.status(200).json({status: 'Success', data: venues})
   }

   static async findById(req, res){
      let venue = await Venue.findByPk(req.params.id)
      res.status(200).json({status: 'Success', data: venue})
   }

   static async update(req,res){
      let { name, address, phone} = req.body
      await Venue.update({
         name: name,
         address: address,
         phone: phone
      },{
         where:{
            id: req.params.id
         }
      })

      res.status(200).json({status: 'Success', data: 'Data berhasil di update'})
   }

   static async destroy(req, res){
      await Venue.destroy({
         where:{
            id: req.params.id
         }
      })

      res.status(200).json({status: 'Success', data: 'Data berhasil di delete'})
   }
}

module.exports = VenueController