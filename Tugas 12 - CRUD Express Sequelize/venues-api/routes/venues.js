var express = require('express');
var router = express.Router();
const venueController = require('../controllers/venue')

router.post('/', venueController.store)
router.get('/', venueController.findAll)
router.get('/:id', venueController.findById)
router.put('/:id', venueController.update)
router.delete('/:id', venueController.destroy)

module.exports = router;
