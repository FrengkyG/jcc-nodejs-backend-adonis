import { register, checkLogin, addSiswa } from "./lib/async";

const args = process.argv.slice(2)

const command = args[0]

switch(command){
   case "register":
      let[nameRaw, passRaw, roleRaw] = args[1].split(',')
      let obj = {}
      obj["name"] = nameRaw
      obj["password"] = passRaw
      obj["role"] = roleRaw
      obj["isLogin"] = false
      register(obj)
      break;
   case "login":
      let [user, pass] = args[1].split(',')
      checkLogin(user, pass)
      break;
   case "addSiswa":
      let [siswa, trainer] = args[1].split(',')
      addSiswa(siswa, trainer)
      break;
}