"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addSiswa = exports.checkLogin = exports.register = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _promises = _interopRequireDefault(require("fs/promises"));

require("core-js/stable");

require("regenerator-runtime/runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var path = 'data.json';

var register = function register(newData) {
  _fs["default"].readFile(path, function (err, data) {
    if (err) console.log(err);
    var dataArr = JSON.parse(data);
    dataArr.push(newData);

    _fs["default"].writeFile(path, JSON.stringify(dataArr), {
      encoding: 'utf-8'
    }, function (err) {
      if (err) console.log(err);
    });
  });
};

exports.register = register;

var checkLogin = function checkLogin(name, pass) {
  _promises["default"].readFile(path).then(function (data) {
    var user = JSON.parse(data);

    for (var i = 0; i < user.length; i++) {
      if (String(name) == user[i]["name"] && String(pass) == user[i]["password"]) {
        user[i]["isLogin"] = true;
        return _promises["default"].writeFile(path, JSON.stringify(user)).then(function () {
          return console.log("Berhasil Login");
        })["catch"]("user/password anda salah");
      } else {}
    }
  });
};

exports.checkLogin = checkLogin;

var addSiswa = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(namaSiswa, namaTrainer) {
    var dataRead, realData, i, j;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return _promises["default"].readFile(path);

          case 2:
            dataRead = _context.sent;
            realData = JSON.parse(dataRead);
            i = 0;

          case 5:
            if (!(i < realData.length)) {
              _context.next = 28;
              break;
            }

            if (!(realData[i]["isLogin"] == true && realData[i]["role"] == "admin")) {
              _context.next = 24;
              break;
            }

            console.log(realData[i]["isLogin"]);
            console.log(realData[i]["role"]);
            j = 0;

          case 10:
            if (!(j < realData.length)) {
              _context.next = 22;
              break;
            }

            console.log('masuk');

            if (!(realData[j]["name"] == String(namaTrainer))) {
              _context.next = 19;
              break;
            }

            console.log('masuk lg');
            realData[j]["students"].push(namaSiswa);
            console.log(realData);
            return _context.abrupt("return", _promises["default"].writeFile(path, JSON.stringify(realData)));

          case 19:
            j++;
            _context.next = 10;
            break;

          case 22:
            _context.next = 25;
            break;

          case 24:
            console.log("Anda tidak terdaftar sebagai admin");

          case 25:
            i++;
            _context.next = 5;
            break;

          case 28:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function addSiswa(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.addSiswa = addSiswa;