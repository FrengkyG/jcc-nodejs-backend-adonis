function range(startNum=0, finishNum=0){
   var result = []
   if(startNum == 0 || finishNum == 0){
      return result
   } else if (startNum < finishNum) {
      for(var i=parseInt(startNum); i<=finishNum; i++){
         result.push(i)
      }
      return result
   } else if(startNum>finishNum){
      for(var i=parseInt(startNum); i>=finishNum; i--){
         result.push(i)
      }
      return result
   }
}

function rangeWithStep(startNum = 0, finishNum = 0, step = 1) {
   var result = []
   if(startNum == 0 || finishNum == 0){
      return result
   } else if (startNum < finishNum) {
      for(var i=parseInt(startNum); i<=finishNum; i+= parseInt(step)){
         result.push(i)
      }
      return result
   } else if(startNum>finishNum){
      for(var i=parseInt(startNum); i>=finishNum; i-= parseInt(step)){
         result.push(i)
      }
      return result
   }
}

function sum(startNum = 0, finishNum = 0, step=1){
   var result=0
   if(startNum == 0 || finishNum == 0){
      return 1
   } else if (startNum < finishNum) {
      for(var i=parseInt(startNum); i<=finishNum; i+= parseInt(step)){
         result+=i
      }
      return result
   } else if(startNum>finishNum){
      for(var i=parseInt(startNum); i>=finishNum; i-= parseInt(step)){
         result+=i
      }
      return result
   }
}

var input = [
   ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
   ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
   ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
   ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(data=input){
   var result = ""
   for(var i=0; i<data.length; i++){
      result += "Nomor ID: " + data[i][0] + "\n"
      result += "Nama Lengkap: " + data[i][1] + "\n"
      result += "TTL: " + data[i][2] + " " + data[i][3] + "\n"
      result += "Hobi: " + data[i][4] + "\n\n"
   }
   return result
}

function balikKata(kata){
   var result = ""
   for(var i=kata.length-1;i>=0;i--){
      result += kata[i]
   }
   return result
}

module.exports = {
   range: range,
   rangeWithStep: rangeWithStep,
   sum: sum,
   dataHandling: dataHandling,
   balikKata: balikKata

}