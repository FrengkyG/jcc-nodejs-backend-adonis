var arrLib = require('./lib/funcLib.js')

var args = process.argv

var range = arrLib.range
var rangeWithStep = arrLib.rangeWithStep
var sum = arrLib.sum
var dataHandling = arrLib.dataHandling
var balikKata = arrLib.balikKata

switch(args[2]){
   case "range":
      console.log(range(args[3], args[4]))
      break;

   case "rangeWithStep":
      console.log(rangeWithStep(args[3], args[4], args[5]))
      break;

   case "sum":
      console.log(sum(args[3], args[4], args[5]))
      break;

   case "dataHandling":
      console.log(dataHandling())
      break;

   case "balikKata":
      console.log(balikKata(args[3]))
      break;

   default:
      console.log('Perintah yang Anda berikan salah')
      break;
}