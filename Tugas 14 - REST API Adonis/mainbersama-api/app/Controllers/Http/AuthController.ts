import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema } from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User'
import UserValidator from 'App/Validators/UserValidator'
import Mail from '@ioc:Adonis/Addons/Mail'
import Database from '@ioc:Adonis/Lucid/Database'

export default class AuthController {
   public async register({ request, response }: HttpContextContract) {
      try {
         const data = await request.validate(UserValidator)

         const newUser = await User.create(data)
         let otp_code: number = Math.floor(100000 + Math.random() * 900000)
         await Database.table('otp_codes').insert({ otp_code: otp_code, user_id: newUser.id })
         await Mail.send((message) => {
            message
               .from('admin@sanberdev.com')
               .to(data.email)
               .subject('Welcome Onboard!')
               .htmlView('mail/otp_verification', { name: data.name, otp_code: otp_code })

         })
         return response.created({ status: 'Registered!', data: newUser, message: 'Silakan lakukan verifikasi OTP yang dikirimkan ke email anda' })
      } catch (error) {
         return response.unprocessableEntity({ message: error.message })
      }
   }

   public async login({ request, response, auth }: HttpContextContract) {
      const userSchema = schema.create({
         email: schema.string(),
         password: schema.string()
      })

      try {
         await request.validate({ schema: userSchema })
         const email = request.input('email')
         const password = request.input('password')

         const token = await auth.use('api').attempt(email, password)

         return response.ok({ message: 'Login Success', token })
      } catch (error) {
         if (error.guard) {
            return response.badRequest({ message: error.message })
         } else {
            return response.badRequest({ message: error.messages })
         }
      }
   }

   public async otp_verification({ request, response }: HttpContextContract) {
      const otp_code = request.input('otp_code')
      const email = request.input('email')
      const user = await User.findByOrFail('email', email)
      const data = await Database.from('otp_codes').where('otp_codes', otp_code).firstOrFail()
      if (user.id == data.user_id) {
         user.isVerified = true
         await user.save()

         return response.ok({ status: 'success', data: 'verfication succeeded!' })
      } else {
         return response.badRequest({ status: 'error', data: 'otp verification failed' })
      }
   }
}
