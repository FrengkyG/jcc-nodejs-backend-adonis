import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateBookingValidator from 'App/Validators/CreateBookingValidator'
import Booking from 'App/Models/Booking'
import Field from 'App/Models/Field'

export default class BookingsController {
  public async store({ request, response, params, auth }: HttpContextContract) {
    const field = await Field.findByOrFail('id', params.field_id)
    const user = auth.user!
    const payload = await request.validate(CreateBookingValidator)

    const booking = new Booking()
    booking.playDateStart = payload.play_date_start
    booking.playDateEnd = payload.play_date_end
    booking.title = payload.title
    booking.related('field').associate(field)
    user.related('myBooking').save(booking)

    return response.created({ status: 'success', data: booking })
  }

  public async join({ response, auth, params }: HttpContextContract) {
    const booking = await Booking.findOrFail(params.id)
    let user = auth.user!

    await booking.related('players').attach([user.id])

    return response.ok({ status: 'success', data: 'successfully join' })
  }

  public async show({ params, response }: HttpContextContract) {
    const booking = await Booking.query().where('id', params.id).preload('players', (userQuery) => {
      userQuery.select(['name', 'email', 'id'])
    }).withCount('players').firstOrFail()
    return response.ok({ status: 'success', data: booking })
  }

  public async unjoin({ response, auth, params }: HttpContextContract) {
    const booking = await Booking.findOrFail(params.id)
    let user = auth.user!

    await booking.related('players').detach([user.id])

    return response.ok({ status: 'success', data: 'successfully unjoin' })
  }

}
