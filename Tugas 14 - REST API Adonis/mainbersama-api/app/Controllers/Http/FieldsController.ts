import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateFieldValidator from 'App/Validators/CreateFieldValidator'
import Field from 'App/Models/Field'
import Venue from 'App/Models/Venue'

export default class FieldsController {
  public async index({ request, response, auth }: HttpContextContract) {
    await auth.use('api').authenticate()
    auth.use('api').isAuthenticated
    if (request.qs().name) {
      let name = request.qs().name
      // let contactsFiltered = await Database.from('fields').select('id', 'name', 'type').where('name', name)
      let contactsFiltered = await Field.findBy('name', name)
      response.status(200).json({ message: 'Success', data: contactsFiltered })
    } else {
      // let contacts = await Database.from('fields').select('id', 'name', 'type')
      let contacts = await Field.all()
      response.status(200).json({ message: 'Success', data: contacts })
    }
  }

  public async store({ request, response, params, auth }: HttpContextContract) {
    await auth.use('api').authenticate()
    auth.use('api').isAuthenticated
    try {
      const venue = await Venue.findByOrFail('id', params.venue_id)
      await request.validate(CreateFieldValidator)
      // let newFieldID = await Database.table('fields').returning('id').insert({
      //   name: request.input('name'),
      //   type: request.input('type'),
      //   venue_id: params.venue_id
      // })

      const newField = new Field()
      newField.name = request.input('name')
      newField.type = request.input('type')
      // newField.venue_id = params.venue_id
      await newField.related('venue').associate(venue)


      // await newField.save()
      return response.created({ message: 'Success', data: newField })
    } catch (error) {
      response.unprocessableEntity({ errors: error.message })
    }
  }

  public async show({ response, params, auth }: HttpContextContract) {
    await auth.use('api').authenticate()
    auth.use('api').isAuthenticated
    // let field = await Database.from('fields').where('id', params.id).select('id', 'name', 'type').first()
    // let field = await Field.find(params.id)
    const field = await Field.query().where('id', params.id).preload('bookings', (bookingQuery) => {
      bookingQuery.select(['title', 'play_date_start', 'play_date_end'])
    }).firstOrFail()
    return response.ok({ message: 'Success', data: field })
  }

  public async update({ request, response, params, auth }: HttpContextContract) {
    await auth.use('api').authenticate()
    auth.use('api').isAuthenticated
    let id = params.id
    try {
      await request.validate(CreateFieldValidator)
      // await Database.from('fields').where('id', id).update({
      //   name: request.input('name'),
      //   type: request.input('type'),
      //   venue_id: params.venue_id
      // })
      // let field = await Database.from('fields').where('id', params.id).select('id', 'name', 'type').first()
      let field = await Field.findOrFail(id)
      field.name = request.input('name')
      field.type = request.input('type')
      field.venueId = params.venue_id
      field.save()
      return response.ok({ message: 'Success', data: field })
    } catch (error) {
      response.unprocessableEntity({ errors: error.messages })
    }

  }

  public async destroy({ params, response, auth }: HttpContextContract) {
    await auth.use('api').authenticate()
    auth.use('api').isAuthenticated
    let id = params.id
    // await Database.from('fields').where('id', id).delete()
    let field = await Field.findOrFail(id)
    await field.delete()
    return response.ok({ message: 'Success' })
  }
}
