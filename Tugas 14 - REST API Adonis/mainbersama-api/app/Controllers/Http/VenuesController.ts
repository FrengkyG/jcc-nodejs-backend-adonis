import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateVenueValidator from 'App/Validators/CreateVenueValidator'
import Venue from 'App/Models/Venue'


export default class VenuesController {
  public async index({ request, response, auth }: HttpContextContract) {
    await auth.use('api').authenticate()
    auth.use('api').isAuthenticated
    if (request.qs().name) {
      let name = request.qs().name
      // let contactsFiltered = await Database.from('venues').select('id', 'name', 'address', 'phone').where('name', name)
      let contactsFiltered = await Venue.findBy('name', name)
      response.status(200).json({ message: 'Success', data: contactsFiltered })
    } else {

      // let venues = await Database.from('venues').select('id', 'name', 'address', 'phone')
      let venues = await Venue.all()
      response.status(200).json({ message: 'Success', data: venues })
    }
  }

  public async store({ request, response, auth }: HttpContextContract) {
    try {
      await auth.use('api').authenticate()
      auth.use('api').isAuthenticated
      const payload = await request.validate(CreateVenueValidator)
      // let newVenueID = await Database.table('venues').returning('id').insert({
      //   name: request.input('name'),
      //   address: request.input('address'),
      //   phone: request.input('phone')
      // })
      let newVenue = new Venue()
      newVenue.name = request.input(payload.name)
      newVenue.address = request.input(payload.address)
      newVenue.phone = request.input(payload.phone)
      await newVenue.save()

      return response.created({ message: 'Success', venueId: newVenue.id })
    } catch (error) {
      return response.unprocessableEntity({ errors: error.messages })
    }

  }

  public async show({ response, params, auth }: HttpContextContract) {
    await auth.use('api').authenticate()
    auth.use('api').isAuthenticated
    // let venues = await Database.from('venues').where('id', params.id).select('id', 'name', 'address', 'phone').first()
    // let venues = await Venue.find(params.id)
    let venues = await Venue.query().where('id', params.id).preload('fields').firstOrFail()
    response.status(200).json({ message: 'Success', data: venues })
  }

  public async update({ request, response, params, auth }: HttpContextContract) {
    await auth.use('api').authenticate()
    auth.use('api').isAuthenticated
    let id = params.id
    try {
      const payload = await request.validate(CreateVenueValidator)
      // await Database.from('venues').where('id', id).update({
      //   name: request.input('name'),
      //   address: request.input('address'),
      //   phone: request.input('phone')
      // })
      // let venues = await Database.from('venues').where('id', params.id).select('id', 'name', 'address', 'phone').first()

      // let venues = await Venue.findOrFail(id)
      // venues.name = request.input(payload.name)
      // venues.address = request.input(payload.address)
      // venues.phone = request.input(payload.phone)
      // venues.save()

      let updated = await Venue.updateOrCreate({ id: id }, payload)
      return response.ok({ message: 'Success', data: updated })
    } catch (error) {
      response.unprocessableEntity({ errors: error.messages })
    }
  }

  public async destroy({ params, response, auth }: HttpContextContract) {
    await auth.use('api').authenticate()
    auth.use('api').isAuthenticated
    let id = params.id
    // await Database.from('venues').where('id', id).delete()
    let venue = await Venue.findOrFail(id)
    await venue.delete()
    return response.ok({ message: 'Success' })
  }
}
