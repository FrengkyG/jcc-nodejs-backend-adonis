// no.1 

const editData = async (update) => {

   let dataJson = await fspromise.readFile('data.json')
   
   .then(dataJson=>{
   
   let data = JSON.parse(dataJson)
   
   let updated={...data,...update}
   
   return fspromise.writeFile('data.json',JSON.stringify(updated))
   
   })
   
   .then(()=>console.log('updated'))
   
   .catch(err=>console.log(err))
   
   }

// no.2
let data = {
   name: 'Bondra',
   nilai:70,
   kelas:'Adonis',
   isLogin:true
   }
   
   
const updateData = (update) => {
   if(update.name){
      data.name = update.name
   } 
   if (update.nilai) {
      data.nilai = update.nilai
   } 

   if (update.kelas) {
      data.kelas = update.kelas

   }
   if(update.isLogin == true || update.isLogin == false){
      data.isLogin = update.isLogin
   }
   
   return data
}
   
   
   // TEST CASES
console.log(updateData({ isLogin: false }))
// Output : { name: 'Bondra', nilai: 70, kelas: 'Adonis', isLogin: false }

console.log(updateData({nilai: 65, kelas: 'Laravel'}))
// Output: { name: 'Bondra', nilai: 65, kelas: 'Laravel', isLogin: true }

// no.3
//SELECT users.name, COUNT(posts.title) as post_count FROM users JOIN posts on users.id = posts.user_id where users.name LIKE 'John Doe'

// no.4
class OlahString {
   // code kamu di sini
   palindrome(str){
      console.log(str == str.split('').reverse().join(''))
    }

    ubahKapital(str){
      console.log(str.charAt(0).toUpperCase() + str.slice(1))
    }
   
}

let funcString = new OlahString()
funcString.palindrome('katak') // Output: true

funcString.palindrome('sanbers') // Output: false

funcString.ubahKapital('asep') // Output : Asep

funcString.ubahKapital('abduh') // Output: Abduh

// no.5
// https://drive.google.com/file/d/1u8NWZNAL4nMPjn3qxszb7x4nPQPIxDJh/view?usp=sharing