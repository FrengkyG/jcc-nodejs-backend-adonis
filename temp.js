function kelompokAngka(arr){
   var tempGanjil = []
   var tempGenap = []
   var tempLipatTiga = []
   var result = []

   for(var i=0; i<arr.length; i++){ 
      if(parseInt(arr[i])%3==0){
         tempLipatTiga.push(arr[i])
      } else if(parseInt(arr[i])%2==0) {
         tempGanjil.push(arr[i])
      } else {
         tempGenap.push(arr[i])
      }
   }
   result.push(tempGenap)
   result.push(tempGanjil)
   result.push(tempLipatTiga)

   return result
}
   
//Test Case

console.log(kelompokAngka([1,2,3,4,5,6,7]))

//Output:[[1,5,7],[2,4],[3,6]]

console.log(kelompokAngka([13,27,11,15]))

//Output:[[13,11],[],[27,15]]

console.log(kelompokAngka([]))

//output:[[],[],[]]







//-------------------------------------------------------

function graduate(arr) {
   // code di sini
   var tempName = []
   var tempScore = []
   var tempClass = []
   var currentScore = 0
   var currentName = ""
   var currentClass = ""
   var output = {}

   for(var i = 0; i<arr.length;i++){
      tempName.push(arr[i].name)
      tempScore.push(arr[i].score)
      tempClass.push(arr[i].class)
   }


   for(var i=0;i<tempScore.length;i++){
      if(tempScore[i] >= 85){
         currentScore = tempScore[i]
         currentName = tempName[i]
         currentClass = tempClass[i]
         output[currentClass] = [{name: currentName, score: currentScore, grade: "mastered"}]
      } else if(tempScore[i] >=60 && tempScore[i] <85) {
         currentScore = tempScore[i]
         currentName = tempName[i]
         currentClass = tempClass[i]
         output[currentClass] = [{name: currentName, score: currentScore, grade: "completed"}]
      } else {
         currentScore = tempScore[i]
         currentName = tempName[i]
         currentClass = tempClass[i]
         output[currentClass] = [{name: currentName, score: currentScore, grade: "participated"}]
      }

   }
   return output
}

   
   
   // TEST CASE
   // Contoh Input
   
   var arr = [
      {name:"Ahmad",score:80, class: "Laravel"},

      {name:"Regi",score:86, class: "Vuejs"},

      {name:"Robert",score:59, class: "Laravel"},

      {name:"Bondra",score:81, class: "Reactjs" }
   ]
   
   
   console.log(graduate(arr))
   
   //Output
   
   // {
   
   // Laravel:[{name:"Ahmad",score:80, grade: "completed"}, { name: "Robert", score: 59, grade: "participated"}],
   
   // Vuejs:[
   
   // {name:"Regi",score:86, grade: "mastered"}
   
   // ],
   
   // Reactjs:[{name:"Bondra",score:81, grade: "completed"}]
   
   // }


// -------------------------------

function hitungVokal(str){
   var count = 0;
   for(var i = 0; i<str.length; i++){
      if(str[i] == 'a' || str[i] == 'i' || str[i] == 'u' || str[i] == 'e' || str[i] == 'o' || str[i] == 'A' || str[i] == 'I' || str[i] == 'U' || str[i] == 'E' || str[i] == 'O'){
         count++
      } else {

      }
   }
   return count
   //code disini
   
}
   
   // Test Cases
   
   console.log(hitungVokal("Adonis"))// Output:3
   
   console.log(hitungVokal("Error"))//Output:2
   
   console.log(hitungVokal("Eureka"))//Output:4
   
   console.log(hitungVokal("Rsch")) // Output: 0




//--------------------------------
function tandaiA(str) {
   // code di sini
   var result =""
   for(var i = 0; i<str.length; i++){
      if(str[i]=='a'){
         result += 'x'
      } else {
         result += str[i]
      }
   }
   return result
}
   
   // Tes Cases
   console.log(tandaiA("abrakadabra")) // xbrxkxdxbrx
   console.log(tandaiA("garuda")) // gxrudx
   console.log(tandaiA("kucing")) // kucing