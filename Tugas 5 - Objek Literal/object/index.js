//no.1
var dataObject = {}

var now = new Date()
var thisYear = now.getFullYear()

function getAge(year){

}
function arrayToObject(arr){
   if(!arr){
      console.log("")
   } else {
      for(var i=0; i<arr.length; i++){
         dataObject.firstName = arr[i][0]
         dataObject.lastName = arr[i][1]
         dataObject.gender = arr[i][2]
         if(parseInt(arr[i][3]) < thisYear ){
            dataObject.age = (thisYear - parseInt(arr[i][3]))
         } else {
            dataObject.age = "Invalid birth year"
         }
         console.log((i+1) + ". " + dataObject.firstName + " " + dataObject.lastName + ": ")
         console.log(dataObject)
      }
   }
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 


// no.2
function shoppingTime(memberId, money) {
   var memberData = {}
   var listPurchased = []
   var change = money
   if(!memberId){
      return "Mohon maaf, toko X hanya berlaku untuk member saja"
   } else if (money < 50000) {
      return "Mohon maaf, uang tidak cukup"
   } else {
      memberData.memberId = memberId
      memberData.money = money
      while(change>=50000){
         if(change>=1500000 ) {
            listPurchased.push('Sepatu Stacattu')
            change -= 1500000
         } else if (change>=500000) {
            listPurchased.push('Baju Zoro')
            change -= 500000
         } else if (change>=250000) {
            listPurchased.push('Baju H&N')
            change -= 250000
         } else if (change>=175000) {
            listPurchased.push('Sweater Uniklooh')
            change -= 175000
         } else if (change>=50000) {
            listPurchased.push('Casing Handphone')
            change -= 50000
            break;
         }
      }
   }

      memberData.listPurchased = listPurchased
      memberData.changeMoney = change

      return memberData
   }
   // you can only write your code here!

  
 // TEST CASES
 console.log(shoppingTime('1820RzKrnWn08', 2475000));
   //{ memberId: '1820RzKrnWn08',
   // money: 2475000,
   // listPurchased:
   //  [ 'Sepatu Stacattu',
   //    'Baju Zoro',
   //    'Baju H&N',
   //    'Sweater Uniklooh',
   //    'Casing Handphone' ],
   // changeMoney: 0 }
 console.log(shoppingTime('82Ku8Ma742', 170000));
 //{ memberId: '82Ku8Ma742',
 // money: 170000,
 // listPurchased:
 //  [ 'Casing Handphone' ],
 // changeMoney: 120000 }
 console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
 console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
 console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// no.3
 function naikAngkot(arrPenumpang) {
   rute = ['A', 'B', 'C', 'D', 'E', 'F'];
   tarifA = 0
   tarifB = 0
   //your code here
   var dataPenumpang = []
   var objectPenumpang = {}
   for(var i = 0; i < arrPenumpang.length; i++) {
      for(var j = 0; j < rute.length; j++) {
         if(arrPenumpang[i][1] == rute[j]) {
            for(var k = 0; k < rute.length; k++) {
               if(arrPenumpang[i][2] == rute[k]){
                  dataPenumpang[i] = {"penumpang" : arrPenumpang[i][0],
                  "naikDari": arrPenumpang[i][1],
                  "tujuan": arrPenumpang[i][2],
                  "bayar": (k-j) * 2000
                  }
               }
            }
         }
      }
   }
   return dataPenumpang
 }
  
 //TEST CASE
 console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
 // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
 //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
  
 console.log(naikAngkot([])); //[]

 // no.4
 function nilaiTertinggi(siswa) {
    var tempName = []
    var tempScore = []
    var tempClass = []
    var currentScore = 0
    var currentName = ""
    var currentClass = ""
    var output = {}

    for(var i = 0; i<siswa.length;i++){
       tempName.push(siswa[i].name)
       tempScore.push(siswa[i].score)
       tempClass.push(siswa[i].class)
    }

    for(var i=0;i<tempClass.length;i++){
       for(var j=tempClass.length-1;j>=0;j--){
         if(tempClass[i] == tempClass[j]){
            if(tempScore[i] > tempScore[j]){
               currentScore = tempScore[i]
               currentName = tempName[i]
               currentClass = tempClass[i]
               output[currentClass] = {name: currentName, score: currentScore}
            } else {
               currentScore = tempScore[j]
               currentName = tempName[j]
               currentClass = tempClass[j]
               output[currentClass] = {name: currentName, score: currentScore}
            }
         }

       }
    }
   return output
   // Code disini
 }
 
 // TEST CASE
 console.log(nilaiTertinggi([
   {
     name: 'Asep',
     score: 90,
     class: 'adonis'
   },
   {
     name: 'Ahmad',
     score: 85,
     class: 'vuejs'
   },
   {
     name: 'Regi',
     score: 74,
     class: 'adonis'
   },
   {
     name: 'Afrida',
     score: 78,
     class: 'reactjs'
   }
 ]));
 
 // OUTPUT:
 
 // {
 //   adonis: { name: 'Asep', score: 90 },
 //   vuejs: { name: 'Ahmad', score: 85 },
 //   reactjs: { name: 'Afrida', score: 78}
 // }
 
 
 console.log(nilaiTertinggi([
   {
     name: 'Bondra',
     score: 100,
     class: 'adonis'
   },
   {
     name: 'Putri',
     score: 76,
     class: 'laravel'
   },
   {
     name: 'Iqbal',
     score: 92,
     class: 'adonis'
   },
   {
     name: 'Tyar',
     score: 71,
     class: 'laravel'
   },
   {
     name: 'Hilmy',
     score: 80,
     class: 'vuejs'
   }
 ]));
 
 // {
 //   adonis: { name: 'Bondra', score: 100 },
 //   laravel: { name: 'Putri', score: 76 },
 //   vuejs: { name: 'Hilmy', score: 80 }
 // }